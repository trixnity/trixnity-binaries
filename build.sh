#!/bin/bash
set -e

OLM_VERSION="3.2.15" # https://gitlab.matrix.org/matrix-org/olm/-/releases
MXE_VERSION="build-2022-04-09" # https://github.com/mxe/mxe/tags
CMAKE_VERSION="3.30.5" # https://cmake.org/download/
ANDROID_NDK_VERSION="r27b" # https://developer.android.com/ndk/downloads/

ENV_DIR="env/"
BUILD_DIR="build/"

###############################################
###############################################
###############################################
# Prepare environment
###############################################
###############################################
###############################################

mkdir -p $ENV_DIR
ENV_DIR=$(realpath $ENV_DIR)

OLM_SOURCES="${ENV_DIR}/olm-$OLM_VERSION"
MXE_SOURCES="${ENV_DIR}/mxe-$MXE_VERSION"
CMAKE_BIN="${ENV_DIR}/cmake-$CMAKE_VERSION"
ANDROID_NDK_BIN="${ENV_DIR}/android-ndk-$ANDROID_NDK_VERSION"

case "$(uname -s)" in
    Linux*)     export HOST=Linux;;
    Darwin*)    export HOST=Mac;;
    *)          export HOST="UNKNOWN"
esac
echo "Detected host is ${HOST}"

if [ ! -d "$OLM_SOURCES" ]; then
  echo "Download and unzip olm"
  wget "https://gitlab.matrix.org/matrix-org/olm/-/archive/$OLM_VERSION/olm-$OLM_VERSION.zip" -O olm.zip
  unzip olm.zip -d "$ENV_DIR"
  rm olm.zip
  if [ $HOST == "Mac" ]; then
    wget "https://raw.githubusercontent.com/leetal/ios-cmake/master/ios.toolchain.cmake" -O "$OLM_SOURCES/ios.toolchain.cmake"
  fi
fi

if [ ! -d "$MXE_SOURCES" ]; then
  if [ $HOST == "Linux" ]; then
    git clone --depth 1 --branch $MXE_VERSION https://github.com/mxe/mxe.git "$MXE_SOURCES"
    (cd "$MXE_SOURCES"; make MXE_USE_CCACHE= MXE_TARGETS="x86_64-w64-mingw32.static" gcc cmake;)
  fi
fi

if [ $HOST == "Linux" ]; then
  export PATH="$MXE_SOURCES/usr/bin/:$PATH"
fi

if [ ! -d "$CMAKE_BIN" ]; then
  echo "Download and install cmake"
  if [ $HOST == "Linux" ]; then
    CMAKE_TAR=cmake-$CMAKE_VERSION-linux-x86_64.tar.gz
  fi
  if [ "$HOST" == "Mac" ]; then
    CMAKE_TAR=cmake-$CMAKE_VERSION-macos-universal.tar.gz
  fi
  wget "https://github.com/Kitware/CMake/releases/download/v$CMAKE_VERSION/$CMAKE_TAR" -O cmake.tar.gz
  tar -xf cmake.tar.gz -C "$ENV_DIR"
  rm cmake.tar.gz
  if [ $HOST == "Linux" ]; then
    mv "$ENV_DIR/cmake-$CMAKE_VERSION-linux-x86_64" "$CMAKE_BIN"
  fi
  if [ "$HOST" == "Mac" ]; then
    mv "$ENV_DIR/cmake-$CMAKE_VERSION-macos-universal/CMake.app/Contents" "$CMAKE_BIN"
  fi
fi
export PATH="$CMAKE_BIN/bin:$PATH"

if [ ! -d "$ANDROID_NDK_BIN" ]; then
  if [ $HOST == "Linux" ]; then
    wget "https://dl.google.com/android/repository/android-ndk-$ANDROID_NDK_VERSION-linux.zip" -O ndk.zip
    unzip ndk.zip -d "$ENV_DIR"
    rm ndk.zip
  fi
fi

echo "env is ready"

###############################################
###############################################
###############################################
# build
###############################################
###############################################
###############################################

rm -rf $BUILD_DIR
mkdir $BUILD_DIR

OLM_BUILD="${BUILD_DIR}olm/"
OLM_BIN="${OLM_BUILD}bin/"
OLM_HEADERS="${OLM_BUILD}headers/"

mkdir -p $OLM_BIN
mkdir -p $OLM_HEADERS

###############################################
# For Trixnity JVM target.
###############################################
echo "build shared libs"
OLM_SHARED_DIR="${OLM_BIN}shared"
olmBuildSharedDesktop() {
  echo "... for target: $1"
  cmake "$OLM_SOURCES" -B$OLM_SHARED_DIR/"$1" -DOLM_TESTS=OFF "${@:2}"
  cmake --build $OLM_SHARED_DIR/"$1"
}
olmBuildSharedDesktopMingw(){
  echo "... for target: $1"
    x86_64-w64-mingw32.static-cmake "$OLM_SOURCES-mingw" -B$OLM_SHARED_DIR/"$1" -DOLM_TESTS=OFF "${@:2}"
    x86_64-w64-mingw32.static-cmake --build $OLM_SHARED_DIR/"$1"
    x86_64-w64-mingw32.static-strip $OLM_SHARED_DIR/"$1"/libolm.dll
    mv $OLM_SHARED_DIR/"$1"/libolm.dll $OLM_SHARED_DIR/"$1"/olm.dll
}
if [ "$HOST" == "Linux" ]; then
  olmBuildSharedDesktop linux-x86-64
  cp -r "$OLM_SOURCES" "$OLM_SOURCES-mingw"
  sed -i 's/\(library(olm\)\b/\1 SHARED/' "$OLM_SOURCES-mingw/CMakeLists.txt"
  olmBuildSharedDesktopMingw win32-x86-64
fi
if [ "$HOST" == "Mac" ]; then
  olmBuildSharedDesktop darwin -DCMAKE_OSX_ARCHITECTURES=x86_64\;arm64
fi

###############################################
# For Trixnity Android target.
###############################################
echo "build shared android libs"
if [ "$HOST" == "Linux" ];
then
  OLM_SHARED_ANDROID_DIR="${OLM_BIN}shared-android"
  olmBuildSharedAndroid() {
    echo "... for target: $1"
    cmake "$OLM_SOURCES" -B$OLM_SHARED_ANDROID_DIR/"$1" -DOLM_TESTS=OFF -DCMAKE_TOOLCHAIN_FILE="$ANDROID_NDK_BIN/build/cmake/android.toolchain.cmake" "${@:2}"
    cmake --build $OLM_SHARED_ANDROID_DIR/"$1"
  }
  olmBuildSharedAndroid armeabi-v7a -DANDROID_ABI=armeabi-v7a
  olmBuildSharedAndroid arm64-v8a -DANDROID_ABI=arm64-v8a
  olmBuildSharedAndroid x86 -DANDROID_ABI=x86
  olmBuildSharedAndroid x86_64 -DANDROID_ABI=x86_64
fi

###############################################
# For Trixnity native targets.
###############################################
echo "build static libs"
OLM_STATIC_DIR="${OLM_BIN}static"
olmBuildStatic() {
  echo "... for target: $1"
  cmake $OLM_SOURCES -B$OLM_STATIC_DIR/"$1" -DOLM_TESTS=OFF -DBUILD_SHARED_LIBS=NO "${@:2}"
  cmake --build $OLM_STATIC_DIR/"$1"
}
if [ "$HOST" == "Linux" ]; then
  olmBuildStatic linux_x64
  olmBuildStatic mingw_x64 -DCMAKE_TOOLCHAIN_FILE=Windows64.cmake
fi
if [ "$HOST" == "Mac" ]; then
  olmBuildStatic macos_arm64 -DCMAKE_OSX_ARCHITECTURES=arm64
  olmBuildStatic macos_x64 -DCMAKE_OSX_ARCHITECTURES=x86_64
  olmBuildStatic ios_simulator_arm64 ""-DCMAKE_TOOLCHAIN_FILE=ios.toolchain.cmake -DPLATFORM=SIMULATORARM64
  olmBuildStatic ios_arm64 -DCMAKE_TOOLCHAIN_FILE=ios.toolchain.cmake -DPLATFORM=OS64
  olmBuildStatic ios_x64 -DCMAKE_TOOLCHAIN_FILE=ios.toolchain.cmake -DPLATFORM=SIMULATOR64
fi

###############################################
# Binaries
###############################################
(cd $OLM_BIN; find . \( -name "libolm.so" -or -name "libolm.a" -or -name "olm.dll" -or -name "libolm.dylib" \) -exec rsync -R --copy-links '{}' tmp \;)
(cd $OLM_BIN; rm -rf shared shared-android static; mv tmp/* ./; rm -r tmp;)

###############################################
# Headers
###############################################

cp -rf "$OLM_SOURCES"/include/* $OLM_HEADERS

echo "built olm"